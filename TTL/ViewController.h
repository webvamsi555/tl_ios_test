#import <UIKit/UIKit.h>
#import "JourneyDateViewController.h"
#import "SendDateTimeProtocol.h"

@interface ViewController : UIViewController <SendDateTimeProtocol>

@end
