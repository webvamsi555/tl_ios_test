Feature: Train Line booking screen 4 test feature

  @ci
  Scenario: As a user I want to see booking summary screen (Happy path)
    Given I am on the home screen
    When I enter "origin" location as "Oxford"
    And I enter "destination" location as "Liverpool"
    And I click on "Search" button
    And I select my journey
    Then I wait to see "Done!"

