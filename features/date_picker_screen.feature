Feature: Train Line date picker screen

  @ci
  Scenario: As a user I want to select departure date
    Given I am on the home screen
    When I enter "origin" location as "Baker"
    And I enter "destination" location as "Liverpool"
    And I selected future date
    Then I should see the "Out" screen

  @wip
  Scenario: As a user I want to select departure date
    Given I am on the home screen
    When I choose return
    And I tap first date button
    Then I should see date picker with "Out" screen title

  @wip
  Scenario: As a user I want to select arrival date
    Given I am on the home screen
    When I choose return
    And I tap second date button
    Then I should see date picker with "Return" screen title

  @wip
  Scenario: As a user I want return journey with past departure date
    Given I am on the home screen
    When I enter my "origin" and " destination" stations
    And I choose open return
    And I selected past departure date
    And I click on search
    Then I should see error message "Departure cannot be in the past"

  @wip
  Scenario: As a user I want return journey with future departure and past arrival date
    Given I am on the home screen
    When I enter my "origin" and " destination" stations
    And I choose open return
    And I selected past departure date
    And I click on search
    Then I should see error message "Arrival cannot be in the past"

  @wip
  Scenario: As a user I want return journey with future departure and current arrival date
    Given I am on the home screen
    When I enter my "origin" and " destination" stations
    And I choose open return
    And I selected past departure date
    And I click on search
    Then I should see error message "Arrival cannot be before departure"