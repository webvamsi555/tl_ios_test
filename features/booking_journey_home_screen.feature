Feature: Train Line home screen test feature

  @ci @v1 @reset
  Scenario: As a user I want one way journey with preferred date
    When I enter "origin" location as "Baker"
    And I enter "destination" location as "Liverpool"
    And I click on "One way" option
    And I selected future date
    And I click on "Done" button
    And I click on "Search" button
    Then I should see the "Journey" screen

  @ci @v1 @reset
  Scenario: As a user I want open return journey with preferred date
    Given I am on the home screen
    When I enter "origin" location as "Baker"
    And I enter "destination" location as "Liverpool"
    And I click on "Open return" option
    And I selected future date
    And I click on "Done" button
    And I click on "Search" button
    Then I should see the "Journey" screen

  @ci @v1 @reset
  Scenario: As a user I should not see list of trains without typing origin and destination
    Given I am on the home screen
    When I click on "Search" button
    Then I should see error message "Origin or destination cannot be empty"

  @ci
  Scenario: As a user I want to check if i can see list of trains by entering origin only
    Given I am on the home screen
    When I enter "origin" location as "London"
    And I click on "Search" button
    Then I should see error message "Origin or destination cannot be empty"

  @ci
  Scenario: As a user I want to check if i can see list of trains by entering destination only
    Given I am on the home screen
    When I enter "destination" location as "Liverpool"
    And I click on "Search" button
    Then I should see error message "Origin or destination cannot be empty"

  @ci
  Scenario: As a user I want to check if i can travel using same station for origin and destination
    Given I am on the home screen
    When I enter "origin" location as "Baker"
    And I enter "destination" location as "Baker"
    And I click on "Search" button
    Then I should see error message "Origin and destination cannot be same"

  @wip
  Scenario: As a user I want return journey with preferred date
    Given I am on the home screen
    When I enter "origin" location as "Baker"
    And I enter "destination" location as "Liverpool"
    And I click on "Return" option
    And I selected future dates from date buttons
    And I click on "Done" button
    And I click on "Search" button
    Then I should see the "Journey" screen

