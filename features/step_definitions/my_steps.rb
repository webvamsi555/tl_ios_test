And(/^I click "([^"]*)" location/) do |arg|
  touch("* marked:'#{arg}'", :offset => {:x => 1, :y => 20})
end


Then(/I enter "([^"]*)" location as "([^"]*)"/) do |arg1,arg2|
  query('UITextField', [{setAutocorrectionType:1}])
  touch("* marked:'#{arg1}'", :offset => {:x => 1, :y => 20})
  fast_enter_text(arg2)
  #touch("* {text CONTAINS '#{arg2}'}", :offset => {:x => 1, :y => 30})
end


Then(/^I use enter_text to enter "([^\"]*)"$/) do |text|
  fast_enter_text(text)
  sleep 1
end

Then(/^I wait to see "([^"]*)"$/) do |arg|
  wait_for_view("* marked:'#{arg}'")
end


Then(/^I should see error message "([^"]*)"$/) do |arg|
  wait_for_view("* marked:'#{arg}'")
end

Given(/^I am on the home screen/) do
  wait_for_view("* marked:'origin'")
end


And(/^I click on "([^"]*)" option/) do |arg|
  query "textField isFirstResponder:1", :resignFirstResponder
  touch "UISegmentLabel text:'#{arg}'"
end

Then(/^I dismiss keyboard$/) do
  query "textField isFirstResponder:1", :resignFirstResponder
end

Then(/^I should see the "([^"]*)" screen$/) do |arg1|
  wait_for_view("* marked:'#{arg1}'")
end

And(/^I selected future date$/) do
  sleep 3
  t=Time.now.strftime("%a %e %b %Y")
  touch("* {text CONTAINS '#{t}'}", :offset => {:x => 1, :y => 40})

  wait_for_view("UIPickerColumnView")
  d = DateTime.parse('3rd Feb 2017')
  picker_set_date_time(d, options = {:animate => true, :picker_id => nil})
end


And(/^I click on "([^"]*)" button$/) do |arg|
  sleep 2
  #touch("* marked:'#{arg}'")
  touch("* {text CONTAINS '#{arg}'}", :offset => {:x => 20, :y => 15})
  sleep 1
end

And(/^I click on "([^"]*)" text$/) do |arg|
  touch("* {text CONTAINS '#{arg}'}", :offset => {:x => 1, :y => 40})
  sleep 10
end

And(/^I select my journey$/) do
  touch("* {text CONTAINS 'Dep'}", :offset => {:x => 1, :y => 40})
end


And(/^I click on "([^"]*)" field/) do |key|

  hash_map = { one_way: "One way",
               open_return: "Open return",
               return:"Return",
               search:"Search",
               faq:"FAQ"
    }
  query "textField isFirstResponder:1", :resignFirstResponder
  touch "UISegmentLabel text: hash_map[:'#{key}']"
end
