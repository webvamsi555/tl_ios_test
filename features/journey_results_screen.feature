Feature: Train Line results screen test feature

  @wip
  Scenario: As a user I want to see list of trains for my one way journey
    Given the app has launched
    When I enter "Oxford" and "Liverpool" in the text fields
    And I choose future date
    And I click on search
    And I see the list of trains with "Oxford" and "Liverpool" details
    Then I should see the list of train containing my destinations

  @wip
  Scenario: As a user I want so see full details in the search list
    Given I search for preferred journey
    When I see my results page with "Journey" screen title
    Then I should see all details with price